# The BGEN repository has moved #
**27/06/2020**

Due to bitbucket.org sunsetting support for mercurial, the BGEN library has now moved to:

https://code.enkre.net/bgen

The new repository is based on the [Fossil SCM](http:www.fossil-scm.org).  I have migrated outstanding issues and wiki pages to the new repository.

The new repository is somewhat experimental at present - if you experience issues with it please let me know,
either using the mailing list at `OXSTATGEN@JISCMAIL.AC.UK`, or by emailing me directly.

Gavin Band